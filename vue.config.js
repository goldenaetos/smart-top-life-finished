const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  transpileDependencies: true
})

// chainWebpack: defineConfig => {
//   if (process.env.NODE_ENV === 'production') {
//     defineConfig.module.rule('vue').uses.delete('cache-loader');
//     defineConfig.module.rule('js').uses.delete('cache-loader');
//     defineConfig.module.rule('ts').uses.delete('cache-loader');
//     defineConfig.module.rule('tsx').uses.delete('cache-loader');
//   }
// }