import axios from "axios";

export async function fetchProductProperties() {
    await axios.get(process.env.VUE_APP_BASE_URL + 'api/product-properties')
        .then(response => {
            localStorage.setItem('knownProductProperties', JSON.stringify(response.data));
        })
        .catch(error => {
            console.log(error);
        })
}