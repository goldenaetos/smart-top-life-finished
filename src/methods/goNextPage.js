export function goNextPage() {
    if (this.currentPage < this.totalPages) {
        ++this.currentPage;
        this.fetchProducts();
    }
}