import axios from "axios";

export async function addProductToCompare(productId) {
    await axios.get(process.env.VUE_APP_BASE_URL + 'api/products/' + productId)
        .then((response) => {
            let imgPath = 'used_images/placeholder2.png';

            if (response.data.images.length) {
                imgPath = response.data.images[0].image_path;
            }

            const product = {
                image: imgPath
            };

            const titles = ['id', 'name', 'price', 'weight', 'length', 'width', 'height', 'characteristics'];

            for (const title of titles) {
                product[title] = response.data[title];
            }

            let propertyIds = response.data.properties.map(property => property.id)

            product.propertyIds = propertyIds;

            this.messageAboutCompare = 'Товар добавлен к сравнению';
            this.showAddToCompareButton = false

            let comparedProducts = JSON.parse(localStorage.getItem('comparedProducts')) || [];
            const isProductExists = comparedProducts.some((p) => p.id === product.id);
            if (isProductExists) {
                return;
            }

            comparedProducts.push(product);

            localStorage.setItem('comparedProducts', JSON.stringify(comparedProducts));
        })
        .catch(error => {
            console.log(error);
        })
}

export function addSingleProductToCompare(product) {
    this.addProductToCompare(product.id);

    product.messageAboutCompare = 'Товар добавлен к сравнению';

    product.showDeleteFromCompareButton = true;
}

export function deleteProductFromCompare(productId) {
    productId = +productId;

    let comparedProducts = JSON.parse(localStorage.getItem('comparedProducts')) || [];

    const index = comparedProducts.findIndex((p) => p.id === productId);

    if (index !== -1) {
        comparedProducts.splice(index, 1);
    }

    this.messageAboutCompare = 'Товар удален из сравнения';

    localStorage.setItem('comparedProducts', JSON.stringify(comparedProducts));

    this.showAddToCompareButton = true;
}

export function deleteSingleProductFromCompare(product) {
    this.deleteProductFromCompare(product.id);

    product.messageAboutCompare = 'Товар удален из сравнения';

    product.showDeleteFromCompareButton = false;
}
