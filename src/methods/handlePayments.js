export function detectCurrencies() {
    let currencies = JSON.parse(localStorage.getItem('currencyRates'));
    currencies.forEach(item => {
        this.currencies.push(item.currency);
    });
}

export function changeRate() {
    if (this.currency === 'UAH') {
        this.rate = 1;
    } else {
        let currencyRates = JSON.parse(localStorage.getItem('currencyRates'));
        const baseEuroRate = currencyRates.find(item => item.currency === 'UAH').rate;

        if (this.currency === 'EUR') {
            this.rate = 1 / baseEuroRate;
        } else {
            const baseRate = currencyRates.find(item => item.currency === this.currency).rate;
            this.rate = 1 / baseEuroRate * baseRate;
        }
    }
}

export function calculateOrderPrice (price) {
    return Math.ceil(price * this.rate * 100) / 100;
}

export function goToPaymentPage(orderId, amount, currency) {
    this.$router.push({
        name: 'payment-page',
        query: {
            orderId: orderId,
            amount: amount,
            currency: currency
        }
    });
}