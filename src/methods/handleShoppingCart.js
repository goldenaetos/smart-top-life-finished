import axios from "axios";

export async function addToShoppingCartInDataBase(token, product, userId, quantity) {
    if (!userId) {
        userId = +localStorage.getItem('authorizedUserId');
    }

    await axios.post(process.env.VUE_APP_BASE_URL + 'api/carts', {
        user_id: userId,
        product_id: product.id,
        quantity: quantity,
        isAdd: true
    }, {
        headers: {Authorization: "Bearer " + token}
    })
        .catch(error => {
            console.log(error);
        });
}

export async function removeFromShoppingCartInDataBase(token, product, userId, quantity) {
    if (!userId) {
        userId = +localStorage.getItem('authorizedUserId');
    }

    await axios.post(process.env.VUE_APP_BASE_URL + 'api/carts', {
        user_id: userId,
        product_id: product.id,
        quantity: quantity,
        isAdd: false
    }, {
        headers: {Authorization: "Bearer " + token}
    })
        .catch(error => {
            console.log(error);
        });
}

export function addToShoppingCart(token, product, userId = null, quantity = 1) {
    this.$nextTick(() => {
        this.$store.commit('showShoppingCartIcon');
    });

    let userRoles = localStorage.getItem('authorizedUserRoles');
    if (userRoles) {
        addToShoppingCartInDataBase(token, product, userId, quantity)
            .catch(error => {
                console.log(error);
            });
    }

    this.productsArraysInShoppingCart = JSON.parse(localStorage.getItem('shoppingCart'));

    const existingProductIndex = this.productsArraysInShoppingCart.findIndex(item => item.id === product.id);
    if (existingProductIndex !== -1) {
        this.productsArraysInShoppingCart[existingProductIndex].quantity += quantity;
    } else {
        let firstImage = [];
        if (product.images && product.images.length > 0) {
            firstImage = [product.images[0]]
        }

        const productWithQuantity = {
            id: product.id,
            quantity: quantity,
            product: {
                id: product.id,
                name: product.name,
                price: product.price,
                images: firstImage
            },
        };
        this.productsArraysInShoppingCart.push(productWithQuantity);
    }

    localStorage.setItem('shoppingCart', JSON.stringify(this.productsArraysInShoppingCart));

    product.messageAboutShoppingCart = 'Товар добавлен в корзину';
}
export function removeFromShoppingCart(token, product, userId = null, quantity = 1) {
    let userRoles = localStorage.getItem('authorizedUserRoles');
    if (userRoles) {
        removeFromShoppingCartInDataBase(token, product, userId, quantity)
            .then(() => {
                if (this.productsArraysInShoppingCart.length < 1) {
                    this.$store.commit('hideShoppingCartIcon');
                }
            })
            .catch(error => {
                console.log(error);
            });
    }

    this.productsArraysInShoppingCart = JSON.parse(localStorage.getItem('shoppingCart'));

    const existingProductIndex = this.productsArraysInShoppingCart.findIndex(item => item.id === product.id);
    if (existingProductIndex !== -1) {
        switch (true) {
            case this.productsArraysInShoppingCart[existingProductIndex].quantity > quantity:
                this.productsArraysInShoppingCart[existingProductIndex].quantity -= quantity;
                break;
            case this.productsArraysInShoppingCart[existingProductIndex].quantity === quantity:
                this.productsArraysInShoppingCart.splice(existingProductIndex, 1);
                if (this.productsArraysInShoppingCart.length < 1) {
                    this.$store.commit('hideShoppingCartIcon');
                }
                break;
            default:
                console.log("You do not have such many products in Your Shopping Cart");
        }
    } else {
        console.log("There are no such products in Your ShoppingCart");
    }

    localStorage.setItem('shoppingCart', JSON.stringify(this.productsArraysInShoppingCart));

    product.messageAboutShoppingCart = '';
}

export async function displayShoppingCartContentFromDataBase(token, userId) {
    await axios.get(process.env.VUE_APP_BASE_URL + 'api/carts/user/' + userId, {
        headers: {
            Authorization: "Bearer " + token
        }
    })
        .then((response) => {
            if (response.data !== 'Carts not found') {
                if (Array.isArray(response.data)) {
                    this.productsInTheShoppingCart = response.data;
                } else {
                    this.productsInTheShoppingCart = [response.data];
                }
            }
        })
        .catch(error => {
                console.log(error);
        });

    let carts = Array.isArray(this.productsInTheShoppingCart) ?
        this.productsInTheShoppingCart : [this.productsInTheShoppingCart];

    const productArray = carts.map(function(cart) {
        let firstImage = [];
        if (cart.images && cart.images.length > 0) {
            firstImage = [cart.images[0]]
        }

        return {
            id: cart.product_id,
            quantity: cart.quantity,
            product: {
                id: cart.product_id,
                name: cart.name,
                price: cart.price,
                images: firstImage
            },
        }
    });

    if (productArray.length > 0) {
        this.$store.commit('showShoppingCartIcon');
    } else {
        this.$store.commit('hideShoppingCartIcon');
    }

    localStorage.setItem('shoppingCart', JSON.stringify(productArray));
}

export function displayShoppingCartContentFromLocalStorage() {
    this.productsInTheShoppingCart = JSON.parse(localStorage.getItem('shoppingCart'));
}

export function calculateTotalPrice() {
    let totalPrice = 0;

    for (const key in this.productsInTheShoppingCart) {
        const product = this.productsInTheShoppingCart[key];
        totalPrice += product.product.price * product.quantity;
    }

    this.totalCartPrice = totalPrice.toFixed(2);
}

