export function goPrevPage() {
    if (this.currentPage > 1) {
        --this.currentPage;
        this.fetchProducts();
    }
}