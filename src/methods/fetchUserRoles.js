import axios from "axios";

export function fetchUserRoles() {
    axios.get(process.env.VUE_APP_BASE_URL + 'api/user-roles')
        .then(response => {
            const sortedArray = Object.entries(response.data.roles)
                .sort((a, b) => parseInt(a[1]) - parseInt(b[1]))
                .map(([id, name]) => ({ id: parseInt(id), name: name }));

            localStorage.setItem('knownUserRoles', JSON.stringify(sortedArray));
        })
        .catch(error => {
            console.log(error);
        })
}
