export function fetchKnownProperties() {
    this.knownProductProperties = JSON.parse(localStorage.getItem('knownProductProperties'));
    this.sortProperties(this.knownProductProperties);
    this.listOfPropertiesToDisplay = this.knownProductProperties.slice();
}

export function sortProperties(properties) {
    if (properties) {
        properties.sort((a, b) => {
            const customNameA = a.custom_name.toUpperCase();
            const customNameB = b.custom_name.toUpperCase();
            if (customNameA < customNameB) {
                return -1;
            }
            if (customNameA > customNameB) {
                return 1;
            }
            return 0;
        });
    }
}

export function addToSelectProperties() {
    if (this.selectedPropertyId && !this.selectedPropertyIds.includes(this.selectedPropertyId)) {
        this.selectedPropertyIds.push(this.selectedPropertyId);
        this.deleteFromKnownProperties(this.selectedPropertyId);
    }
    this.selectedPropertyId = null;
}

export function deleteFromKnownProperties(id) {
    const index = this.knownProductProperties.findIndex(property => property.id === id);
    if (index !== -1) {
        this.knownProductProperties.splice(index, 1);
    }
}

export function getPropertyById(id) {
    return this.listOfPropertiesToDisplay.find(property => property.id === id) || {};
}

export function comeBackProperty(id) {
    const index = this.selectedPropertyIds.indexOf(id);
    if (index !== -1) {
        this.selectedPropertyIds.splice(index, 1);
        const property = this.getPropertyById(id);
        if (property) {
            this.knownProductProperties.push(property);
            this.sortProperties(this.knownProductProperties);
        }
    }
}
