import { createStore } from 'vuex'

export default createStore({
  state: {
    shoppingCartIconIsShown: false,
    shoppingCartContentIsShown: false
  },
  getters: {
  },
  mutations: {
    showShoppingCartIcon(state) {
      state.shoppingCartIconIsShown = true;
    },
    hideShoppingCartIcon(state) {
      state.shoppingCartIconIsShown = false;
    },
    showShoppingCartContent(state) {
      state.shoppingCartContentIsShown = true;
    },
    hideShoppingCartContent(state) {
      state.shoppingCartContentIsShown = false;
    }
  },
  actions: {
  },
  modules: {
  }
})
