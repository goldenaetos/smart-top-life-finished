import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'

const routes = [
  {
    path: '/',
    name: 'home',
    meta: {layout: 'MainLayout'},
    component: HomeView
  },

  {
    path: '/payment-page',
    name: 'payment-page',
    meta: {layout: 'MainLayout'},
    component: () => import('../views/PaymentPage.vue')
  },

  {
    path: '/login',
    name: 'login',
    meta: {layout: 'AuthLayout'},
    component: () => import('../views/LoginView.vue')
  },
  {
    path: '/register',
    name: 'register',
    meta: {layout: 'AuthLayout'},
    component: () => import('../views/RegisterView.vue')
  },
  {
    path: '/google-callback',
    name: 'google-callback',
    meta: {layout: 'WhiteLayout'},
    component: () => import('../views/GoogleCallbackVue.vue')
  },
  {
    path: '/logout',
    name: 'logout',
    meta: {layout: 'AuthLayout'},
    component: () => import('../views/LogoutView.vue')
  },

  {
    path: '/adminpanel',
    name: 'adminpanel',
    meta: {layout: 'MainLayout', requiresAdmin: true},
    component: () => import('../views/AdminPanel.vue')
  },
  {
    path: '/user-roles',
    name: 'user-roles',
    meta: {layout: 'MainLayout', requiresAdmin: true},
    component: () => import('../views/UserRoles.vue')
  },

  {
    path: '/products',
    name: 'products',
    meta: {layout: 'MainLayout'},
    component: () => import('../views/products/AllProducts.vue')
  },
  {
    path: '/products/add',
    name: 'create-product',
    meta: {layout: 'MainLayout', requiresAdmin: true},
    component: () => import('../views/products/CreateProduct.vue')
  },
  {
    path: '/products/show/:id',
    name: 'single-product',
    meta: {layout: 'MainLayout'},
    component: () => import('../views/products/ShowProduct.vue')
  },
  {
    path: '/products/update/:id',
    name: 'update-product',
    meta: {layout: 'MainLayout', requiresAdmin: true},
    component: () => import('../views/products/UpdateProduct.vue')
  },
  {
    path: '/products/compare',
    name: 'compare-products',
    meta: {layout: 'MainLayout'},
    component: () => import('../views/products/CompareProducts.vue')
  },
  {
    path: '/categories',
    name: 'categories',
    meta: {layout: 'MainLayout'},
    component: () => import('../views/categories/AllCategories.vue')
  },
  {
    path: '/categories/add',
    name: 'create-category',
    meta: {layout: 'MainLayout', requiresAdmin: true},
    component: () => import('../views/categories/CreateCategory.vue')
  },
  {
    path: '/categories/show/:id',
    name: 'single-category',
    meta: {layout: 'MainLayout'},
    component: () => import('../views/categories/ShowCategory.vue')
  },
  {
    path: '/categories/update/:id',
    name: 'update-category',
    meta: {layout: 'MainLayout', requiresAdmin: true},
    component: () => import('../views/categories/UpdateCategory.vue')
  },

  {
    path: '/carts',
    name: 'carts',
    meta: {layout: 'MainLayout'},
    component: () => import('../views/carts/AllCarts.vue')
  },
  {
    path: '/carts/add',
    name: 'create-cart',
    meta: {layout: 'MainLayout', requiresAdmin: true},
    component: () => import('../views/carts/CreateCart.vue')
  },
  {
    path: '/carts/show/:id',
    name: 'single-cart',
    meta: {layout: 'MainLayout'},
    component: () => import('../views/carts/ShowCart.vue')
  },

  {
    path: '/orders',
    name: 'orders',
    meta: {layout: 'MainLayout'},
    component: () => import('../views/orders/AllOrders.vue')
  },
  {
    path: '/orders/add',
    name: 'create-order',
    meta: {layout: 'MainLayout', requiresAdmin: true},
    component: () => import('../views/orders/CreateOrder.vue')
  },
  {
    path: '/orders/show/:id',
    name: 'single-order',
    meta: {layout: 'MainLayout'},
    component: () => import('../views/orders/ShowOrder.vue')
  },
  {
    path: '/orders/update/:id',
    name: 'update-order',
    meta: {layout: 'MainLayout', requiresAdmin: true},
    component: () => import('../views/orders/UpdateOrder.vue')
  },

  {
    path: '/users',
    name: 'users',
    meta: {layout: 'MainLayout', requiresAdmin: true},
    component: () => import('../views/users/AllUsers.vue')
  },
  {
    path: '/users/add',
    name: 'create-user',
    meta: {layout: 'MainLayout', requiresAdmin: true},
    component: () => import('../views/users/CreateUser.vue')
  },
  {
    path: '/users/show/:id',
    name: 'single-user',
    meta: {layout: 'MainLayout'},
    component: () => import('../views/users/ShowUser.vue')
  },
  {
    path: '/users/show/myself',
    name: 'single-user-myself',
    meta: {layout: 'MainLayout'},
    component: () => import('../views/users/ShowUserMyself.vue')
  },
  {
    path: '/users/update/:id',
    name: 'update-user',
    meta: {layout: 'MainLayout'},
    component: () => import('../views/users/UpdateUser.vue')
  },

  {
    path: '/:pathMatch(.*)*',
    name: 'notfound',
    meta: {layout: 'MainLayout'},
    component: () => import('../views/NotFound.vue')
  },
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

router.beforeEach((to, from, next) => {
  const userRoles= localStorage.getItem('authorizedUserRoles');
  let isAdmin = false;

  if (userRoles && userRoles.includes('admin')) {
    isAdmin = true;
  }

  if (to.meta.requiresAdmin && !isAdmin) {
    next({ name: 'home'});
  } else {
    next();
  }
});

export default router
